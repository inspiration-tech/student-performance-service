INSERT INTO students (id, first_name, last_name, patronymic, group_number, is_active, created_at) VALUES (1, 'Котов', 'Иван', 'Иванович', 125, true, '2022-11-08 22:51:58.000000');
INSERT INTO students (id, first_name, last_name, patronymic, group_number, is_active, created_at) VALUES (3, 'Коровина', 'Елена', 'Леонидовна', 125, true, '2022-11-09 03:46:33.519000');

INSERT INTO courses (id, name, start_date, end_date, is_active, created_at) VALUES (3, 'Основы безопасности жизнедеятельности', '2022-10-17 00:00:00.000000', '2022-12-17 23:59:59.000000', true, '2022-11-09 00:44:32.493000');
INSERT INTO courses (id, name, start_date, end_date, is_active, created_at) VALUES (4, 'Основы безопасности жизнедеятельности 2', '2022-10-19 00:00:00.000000', '2022-12-19 23:59:59.000000', true, '2022-11-09 03:29:37.499000');
INSERT INTO courses (id, name, start_date, end_date, is_active, created_at) VALUES (5, 'Основы безопасности жизнедеятельности 3', '2022-10-29 00:00:00.000000', '2022-12-29 23:59:59.000000', true, '2022-11-09 03:29:50.096000');

INSERT INTO students_courses (student_id, course_id) VALUES (1, 3);
INSERT INTO students_courses (student_id, course_id) VALUES (1, 4);
INSERT INTO students_courses (student_id, course_id) VALUES (3, 3);
INSERT INTO students_courses (student_id, course_id) VALUES (1, 5);

INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (1, 3, 'Правила оказания первой помощи', '2022-11-28 17:00:00.000000', 5, '2022-11-09 00:48:24.197000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (2, 3, 'Правила оказания первой помощи 2', '2022-11-28 17:00:00.000000', 10, '2022-11-09 03:30:00.214000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (3, 3, 'Правила оказания первой помощи 3', '2022-11-28 17:00:00.000000', 5, '2022-11-09 03:30:07.406000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (4, 3, 'Правила оказания первой помощи 4', '2022-11-28 17:00:00.000000', 5, '2022-11-09 03:30:13.266000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (5, 4, 'Правила оказания первой помощи 5', '2022-11-28 17:00:00.000000', 5, '2022-11-09 03:30:28.138000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (6, 4, 'Правила оказания первой помощи 6', '2022-11-28 17:00:00.000000', 5, '2022-11-09 03:30:36.396000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (7, 4, 'Правила оказания первой помощи 7', '2022-11-28 17:00:00.000000', 15, '2022-11-09 03:30:44.883000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (8, 4, 'Правила оказания первой помощи 8', '2022-11-28 17:00:00.000000', 10, '2022-11-09 03:30:55.430000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (9, 5, 'Правила оказания первой помощи 9', '2022-11-28 17:00:00.000000', 10, '2022-11-09 03:31:02.619000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (10, 5, 'Правила оказания первой помощи 10', '2022-11-28 17:00:00.000000', 5, '2022-11-09 03:31:11.025000');
INSERT INTO lessons (id, course_id, name, scheduled_date, maximum_score, created_at) VALUES (11, 5, 'Правила оказания первой помощи 11', '2022-11-28 17:00:00.000000', 5, '2022-11-09 03:31:14.806000');

INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (1, 1, 1, 5, '2022-11-09 00:51:02.429000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (4, 3, 1, 5, '2022-11-09 03:45:06.375000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (5, 4, 1, 5, '2022-11-09 03:45:09.259000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (6, 5, 1, 5, '2022-11-09 04:19:50.000000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (8, 2, 3, 3, '2022-11-09 03:44:12.575000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (9, 3, 3, 5, '2022-11-09 03:45:06.375000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (10, 4, 3, 5, '2022-11-09 03:45:09.259000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (7, 1, 3, 5, '2022-11-09 04:32:43.000000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (3, 2, 1, 3, '2022-11-09 03:44:12.575000');
INSERT INTO marks (id, lesson_id, student_id, score, created_at) VALUES (11, 6, 1, 4, '2022-11-10 18:03:08.000000');

INSERT INTO ratings (id, student_id, course_id, score, created_at, updated_at, is_passed, percentage) VALUES (1, 1, 5, 0, '2022-11-10 19:23:32.766666', null, false, 0);
INSERT INTO ratings (id, student_id, course_id, score, created_at, updated_at, is_passed, percentage) VALUES (2, 1, 4, 9, '2022-11-10 19:23:32.766666', null, false, 0.26);
INSERT INTO ratings (id, student_id, course_id, score, created_at, updated_at, is_passed, percentage) VALUES (3, 1, 3, 18, '2022-11-10 19:23:32.766666', null, true, 0.72);
INSERT INTO ratings (id, student_id, course_id, score, created_at, updated_at, is_passed, percentage) VALUES (4, 3, 3, 18, '2022-11-10 19:23:32.766666', null, true, 0.72);

SELECT setval('students_id_seq', COALESCE((SELECT MAX(id)+1 FROM students), 1), false);
SELECT setval('ratings_id_seq', COALESCE((SELECT MAX(id)+1 FROM ratings), 1), false);
SELECT setval('courses_id_seq', COALESCE((SELECT MAX(id)+1 FROM courses), 1), false);
SELECT setval('lessons_id_seq', COALESCE((SELECT MAX(id)+1 FROM lessons), 1), false);
SELECT setval('marks_id_seq', COALESCE((SELECT MAX(id)+1 FROM marks), 1), false);
