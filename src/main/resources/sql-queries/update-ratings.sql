DELETE FROM ratings;

SELECT setval('ratings_id_seq', COALESCE((SELECT MAX(id)+1 FROM ratings), 1), false);

insert into ratings (student_id, course_id, score, percentage, is_passed, created_at)  (
select st_id,
       crs_id,
--        maximum_score,
       actual_sum,
       round(cast((actual_sum / maximum_score) as numeric), 2) as rating,
       round(cast((actual_sum / maximum_score) as numeric), 2) >= 0.7 as is_passed,
       now() as created_at
from (select st_id,
             crs_id,
             sum(maximum_score) as maximum_score,
             cast(sum(
                     case
                         when l_mark is null then
                             0
                         else
                             l_mark
                         end
                 ) as float) as actual_sum
      from (select *,
                   c.id as crs_id,
                   m.score as l_mark,
                   s.id as st_id
            from courses c
                     join students_courses sc on c.id = sc.course_id
                     join students s on sc.student_id = s.id
                     join lessons l on c.id = l.course_id
                     left join marks m on l.id = m.lesson_id and s.id = m.student_id) as tbl
      group by st_id, crs_id) as tbl2);
