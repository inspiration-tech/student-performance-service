package ru.education.courses.controller;

import liquibase.repackaged.org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import ru.education.courses.service.http.response.HttpResponseError;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ControllerUtils {

    public Map<String, String> getErrorsAsMap(Errors bindingResult) {
        Collector<FieldError, ?, Map<String, String>> collector = Collectors.toMap(
            FieldError::getField,
            FieldError::getDefaultMessage
        );

        return bindingResult.getFieldErrors().stream().collect(collector);
    }

    public ArrayList<HttpResponseError> getFieldErrorList(Errors bindingResult) {
        Stream<HttpResponseError> stream = bindingResult.getFieldErrors().stream().map(
            fieldError -> new HttpResponseError(fieldError.getDefaultMessage(), fieldError.getField())
        );

        return stream.collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<HttpResponseError> getGlobalErrorList(Errors bindingResult) {
        Stream<HttpResponseError> stream2 = bindingResult.getGlobalErrors().stream().map(
            globalError -> {
                // Exception e = globalError.unwrap(Exception.class);
                return new HttpResponseError(globalError.getDefaultMessage());
            }
        );

        return stream2.collect(Collectors.toCollection(ArrayList::new));
    }

    public ArrayList<HttpResponseError> getAllErrorList(Errors bindingResult) {
        ArrayList<HttpResponseError> errorList = getFieldErrorList(bindingResult);
        ArrayList<HttpResponseError> globalErrorList = getGlobalErrorList(bindingResult);

        errorList.addAll(globalErrorList);

        return errorList;
    }

    public String generateExceptionIdentifier(Exception e) {
        String hashedString = e.getClass() + DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        assert digest != null;
        byte[] bytes1 = digest.digest(hashedString.getBytes(StandardCharsets.UTF_8));

        return DatatypeConverter.printHexBinary(bytes1).toLowerCase();
    }

}
