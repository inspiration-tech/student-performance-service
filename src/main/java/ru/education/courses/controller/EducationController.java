package ru.education.courses.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.education.courses.service.http.response.HttpResponseJson;
import ru.education.courses.service.http.response.HttpResponseJsonFactory;
import ru.education.courses.service.students.StudentService;
import ru.education.courses.service.students.StudentServiceException;

import java.util.Map;

@RestController
@RequestMapping(path = "/api", produces = "application/json")
@Api(value="api", tags=("api"))
public class EducationController {

    protected HttpResponseJson respJson;
    protected HttpResponseJsonFactory responseJsonFactory;
    protected ControllerUtils controllerUtils;
    private StudentService studentService;

    @Autowired
    public void setDependencies(
            HttpResponseJsonFactory responseJsonFactory,
            ControllerUtils controllerUtils,
            StudentService studentService
    ) {
        this.responseJsonFactory = responseJsonFactory;
        this.controllerUtils = controllerUtils;
        this.studentService = studentService;
    }

    /**
     * получение данных по студенту (курс, рейтинговый балл, зачтен ли курс)
     */
    @GetMapping({"/get-student-data"})
    @ApiOperation(
            value="Получение полных данных по студенту и курсам, на которые он записан",
            notes="На вход принимается json либо с полем \"id\", либо с тремя полями ФИО: \"firstName\" (имя), \"lastName\" (фамилия), \"patronymic\" (отчество)",
            nickname = "getStudentData"
    )
    public ResponseEntity<String> getStudentData(@RequestBody Map<String, Object> inputJson) throws StudentServiceException {

        Map<String, Object> studentData = studentService.getStudentData(inputJson);

        respJson = responseJsonFactory.createSuccess("Данные по студенту получены");
        respJson.setData(studentData);

        return ResponseEntity.ok(respJson.toString());
    }

    /**
     * создание или обновление обучающегося
     */
    @PostMapping({"/update-student"})
    @ApiOperation(
            value="Создание или обновление студента",
            notes="На вход принимается json с тремя полями ФИО: \"firstName\" (имя), \"lastName\" (фамилия), \"patronymic\" (отчество), а также номером группы \"groupNumber\"",
            nickname = "updateStudent"
    )
    public ResponseEntity<String> updateStudent(@RequestBody Map<String, Object> inputJson) {

        studentService.updateStudent(inputJson);

        respJson = responseJsonFactory.createSuccess("Данные по студенту обновлены");
        return ResponseEntity.ok(respJson.toString());
    }

    /**
     * создание или обновление занятия
     */
    @PostMapping({"/update-lesson"})
    @ApiOperation(
            value="Создание или обновление занятия",
            notes="На вход принимается json со следующими полями: id курса \"courseId\", название занятия \"lessonName\", максимальный балл \"maximumScore\", дата проведения \"scheduledDate\"",
            nickname = "updateLesson"
    )
    public ResponseEntity<String> updateLesson(@RequestBody Map<String, Object> inputJson) throws StudentServiceException {

        studentService.updateLesson(inputJson);

        respJson = responseJsonFactory.createSuccess("Данные по занятию обновлены");
        return ResponseEntity.ok(respJson.toString());
    }

    /**
     * создание или обновление курса
     */
    @PostMapping({"/update-course"})
    @ApiOperation(
            value="Создание или обновление курса",
            notes="На вход принимается json со следующими полями: название курса \"courseName\", дата начала \"startDate\" (YYYY-MM-DD HH:MM:SS), дата окончания \"endDate\" (YYYY-MM-DD HH:MM:SS)",
            nickname = "updateCourse"
    )
    public ResponseEntity<String> updateCourse(@RequestBody Map<String, Object> inputJson) throws StudentServiceException {

        studentService.updateCourse(inputJson);

        respJson = responseJsonFactory.createSuccess("Данные по курсу обновлены");
        return ResponseEntity.ok(respJson.toString());
    }

    /**
     * выставление или изменение оценки обучающегося за занятие
     */
    @PostMapping({"/set-mark"})
    @ApiOperation(
            value="Выставление или изменение оценки обучающегося за занятие",
            notes="На вход принимается json со следующими полями: id студента \"studentId\", id занятия \"lessonId\", балл-оценка \"score\"",
            nickname = "setMark"
    )
    public ResponseEntity<String> setMark(@RequestBody Map<String, Object> inputJson) {

        studentService.setMark(inputJson);

        respJson = responseJsonFactory.createSuccess("Оценка студента обновлена");
        return ResponseEntity.ok(respJson.toString());
    }

    /**
     * связывание обучающегося с курсом
     */
    @PostMapping({"/enroll-student"})
    @ApiOperation(
            value="Связывание обучающегося с курсом / запись на курс",
            notes="На вход принимается json со следующими полями: id студента \"studentId\", id курса \"courseId\"",
            nickname = "enrollStudent"
    )
    public ResponseEntity<String> enrollStudent(@RequestBody Map<String, Object> inputJson) throws StudentServiceException {

        studentService.enrollStudentForCourse(inputJson);

        respJson = responseJsonFactory.createSuccess("Студент записан на указанный курс");
        return ResponseEntity.ok(respJson.toString());
    }

}
