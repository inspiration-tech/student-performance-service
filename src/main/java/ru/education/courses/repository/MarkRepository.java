package ru.education.courses.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.education.courses.model.Mark;

@Repository
public interface MarkRepository extends CrudRepository<Mark, Long> {

    @Query(value = "select * from marks m where \n"+
            " m.student_id = :student_id and \n"+
            " m.lesson_id = :lesson_id", nativeQuery = true)
    Mark findByStudentIdAndLessonId(
            @Param("student_id") Long studentId,
            @Param("lesson_id") Long lessonId);

}
