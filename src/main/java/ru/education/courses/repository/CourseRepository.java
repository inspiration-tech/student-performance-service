package ru.education.courses.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.education.courses.model.Course;
import ru.education.courses.service.students.dto.CourseDataDto;

import java.util.List;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {

    @Query(value = "select * from courses c where \n"+
            " c.name = :name", nativeQuery = true)
    Course findByName(
            @Param("name") String courseName);

    @Query(name = "get_student_courses", nativeQuery = true)
    List<CourseDataDto> findDTOs(
            @Param("student_id") Long studentId
    );

}
