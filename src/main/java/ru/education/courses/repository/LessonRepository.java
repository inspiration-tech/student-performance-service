package ru.education.courses.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.education.courses.model.Lesson;

@Repository
public interface LessonRepository extends CrudRepository<Lesson, Long> {

    @Query(value = "select * from lessons l where \n"+
            " l.name = :name and \n"+
            " l.course_id = :course_id", nativeQuery = true)
    Lesson findByNameAndCourseId(
            @Param("name") String courseName,
            @Param("course_id") Long courseId);

}
