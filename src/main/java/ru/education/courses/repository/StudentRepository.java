package ru.education.courses.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.education.courses.model.Student;
import ru.education.courses.service.students.dto.StudentDataDto;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    @Query(value = "select * from students s where \n"+
            " s.first_name = :first_name and \n"+
            " s.last_name = :last_name and \n"+
            " s.patronymic = :patronymic", nativeQuery = true)
    Student findByFullName(
            @Param("first_name") String firstName,
            @Param("last_name") String lastName,
            @Param("patronymic") String patronymic);

    @Query(value = "select * from students s where \n"+
            " s.first_name = :first_name and \n"+
            " s.last_name = :last_name and \n"+
            " s.patronymic = :patronymic and \n"+
            " s.group_number = :group_number", nativeQuery = true)
    Student findByFullNameAndGroupNumber(
            @Param("first_name") String firstName,
            @Param("last_name") String lastName,
            @Param("patronymic") String patronymic,
            @Param("group_number") int groupNumber);


    @Query("SELECT new ru.education.courses.service.students.dto.StudentDataDto(s) FROM Student s WHERE s.id = :id AND s.isActive = TRUE")
    StudentDataDto getStudentDataById(@Param("id") Long id);

    @Query("SELECT new ru.education.courses.service.students.dto.StudentDataDto(s) FROM Student s WHERE s.firstName = :firstName AND s.lastName = :lastName AND s.patronymic = :patronymic AND s.isActive = TRUE")
    StudentDataDto getStudentDataByFullName(
            @Param("firstName") String firstName,
            @Param("lastName") String lastName,
            @Param("patronymic") String patronymic
    );

}
