package ru.education.courses.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.education.courses.model.Rating;

@Repository
public interface RatingRepository extends CrudRepository<Rating, Long> {

}
