package ru.education.courses.handlers;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import org.hibernate.dialect.PostgreSQL10Dialect;


public class CustomPostgreSQL10Dialect extends PostgreSQL10Dialect {

    public CustomPostgreSQL10Dialect() {
        super();
        this.registerHibernateType(2003, StringArrayType.class.getName());
    }

}
