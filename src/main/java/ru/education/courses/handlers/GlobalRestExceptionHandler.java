package ru.education.courses.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.education.courses.controller.ControllerUtils;
import ru.education.courses.service.http.response.HttpResponseError;
import ru.education.courses.service.http.response.HttpResponseJson;
import ru.education.courses.service.http.response.HttpResponseJsonFactory;
import ru.education.courses.service.students.StudentServiceException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@RestControllerAdvice(basePackages = {"ru.education.courses.controller"})
@Order(Ordered.HIGHEST_PRECEDENCE)
class GlobalRestExceptionHandler {

    private ControllerUtils controllerUtils;
    private HttpResponseJsonFactory responseJsonFactory;
    private static Logger logger = LoggerFactory.getLogger(GlobalRestExceptionHandler.class);

    @Autowired
    public GlobalRestExceptionHandler(
            ControllerUtils controllerUtils,
            HttpResponseJsonFactory responseJsonFactory
    ) {
        this.controllerUtils = controllerUtils;
        this.responseJsonFactory = responseJsonFactory;
    }


    @ExceptionHandler(StudentServiceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleBidCreationException(HttpServletRequest req, StudentServiceException e) {
        String exceptionIdentifier = controllerUtils.generateExceptionIdentifier(e);

        logger.error("[GlobalExceptionHandler][StudentServiceException]["+exceptionIdentifier+"]", e);

        HttpResponseJson respJson = responseJsonFactory.createFailed(e.getMessage());

        return ResponseEntity
                .badRequest()
                .header("Content-Type", "application/json")
                .body(respJson.toString());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<String> handleUncaughtException(HttpServletRequest req, Exception e) {
        String exceptionIdentifier = controllerUtils.generateExceptionIdentifier(e);

        logger.error("[Action Controller][Uncaught Exception]["+exceptionIdentifier+"]", e);

        HttpResponseError responseError = new HttpResponseError("При обработке данных произошла ошибка. Пожалуйста, проверьте введённые данные.");
        responseError.setDetails(new HashMap<>(){{
            put("identifier", exceptionIdentifier);
        }});

        HttpResponseJson respJson = responseJsonFactory.createFailed(responseError);

        return ResponseEntity
                .internalServerError()
                .header("Content-Type", "application/json")
                .body(respJson.toString());
    }

}