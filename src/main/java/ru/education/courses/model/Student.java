package ru.education.courses.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;

/**
 * Студент
 */
@Entity
@Getter
@Setter
@Table(name = "students")
public class Student extends AbstractEntity {

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "group_number")
    private int groupNumber;

    @Column(name = "is_active")
    private boolean isActive;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="students_courses",
            joinColumns=@JoinColumn(name="student_id"),
            inverseJoinColumns=@JoinColumn(name="course_id")
    )
    private Set<Course> courses = new HashSet<>();

    @OneToMany(mappedBy="student", fetch = FetchType.EAGER)
    @Getter(AccessLevel.NONE)
    private List<Mark> marks = new ArrayList<>();

    public Student() {}

    public Student(Long id) {
        this.id = id;
    }

}
