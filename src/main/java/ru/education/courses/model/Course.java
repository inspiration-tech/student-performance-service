package ru.education.courses.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import ru.education.courses.service.students.dto.CourseDataDto;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.*;

/**
 * Курс
 */
@Entity
@Getter
@Setter
@Table(name = "courses")
@NamedNativeQuery(
        name = "get_student_courses",
        query =
                "select\n" +
            "        c.id as id,\n" +
            "        c.name as courseName,\n" +
            "        concat(cast(c.start_date as date), ' ', cast(c.start_date as time)) as startDate,\n" +
            "        concat(cast(c.end_date as date), ' ', cast(c.end_date as time)) as endDate,\n" +
            "        r.score as score,\n" +
            "        r.is_passed as isPassed,\n" +
            "        r.percentage as percentage,\n" +
            "        concat(cast(r.created_at as date), ' ', cast(r.created_at as time)) as ratingCreatedAt\n" +
            "    from courses c\n" +
            "             join students_courses sc on c.id = sc.course_id\n" +
            "               join ratings r on r.student_id = sc.student_id and r.course_id = c.id\n" +
            "    where sc.student_id = :student_id and c.is_active = true",
        resultSetMapping = "course_data_dto"
)
@SqlResultSetMapping(
        name = "course_data_dto",
        classes = @ConstructorResult(
                targetClass = CourseDataDto.class,
                columns = {
                        @ColumnResult(name = "id", type = BigInteger.class),
                        @ColumnResult(name = "courseName", type = String.class),
                        @ColumnResult(name = "startDate", type = String.class),
                        @ColumnResult(name = "endDate", type = String.class),
                        @ColumnResult(name = "score", type = Integer.class),
                        @ColumnResult(name = "isPassed", type = Boolean.class),
                        @ColumnResult(name = "percentage", type = Float.class),
                        @ColumnResult(name = "ratingCreatedAt", type = String.class)
                }
        )
)
public class Course extends AbstractEntity {

    @Column(name = "name", columnDefinition = "VARCHAR(255)", unique = true)
    private String name;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "is_active")
    private boolean isActive;

    @ManyToMany(mappedBy = "courses")
    @Getter(AccessLevel.NONE)
    private Set<Student> students = new HashSet<>();

    @OneToMany(mappedBy="course")
    @Getter(AccessLevel.NONE)
    private List<Lesson> lessons = new ArrayList<>();

    public Course() {}

    public Course(Long id) {
        this.id = id;
    }

}
