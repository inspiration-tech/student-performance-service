package ru.education.courses.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Занятие
 */
@Entity
@Getter
@Setter
@Table(name = "lessons")
public class Lesson extends AbstractEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "scheduled_date")
    private Date scheduledDate;

    @Column(name = "maximum_score")
    private int maximumScore;

    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
    private Course course;

    public Lesson() {}

    public Lesson(Long id) {
        this.id = id;
    }

}
