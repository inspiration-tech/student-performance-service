package ru.education.courses.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Рейтинг/успеваемость студента
 */
@Entity
@Getter
@Setter
@Table(name = "ratings")
public class Rating extends AbstractEntity {

    @Column(name = "score")
    private int score;

    @Column(name = "percentage")
    private float percentage;

    @Column(name = "is_passed")
    private boolean isPassed;

    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "id", nullable = false)
    private Course course;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false)
    private Student student;

}
