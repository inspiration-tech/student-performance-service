package ru.education.courses.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Оценка
 */
@Entity
@Getter
@Setter
@Table(name = "marks")
public class Mark extends AbstractEntity {

    @Column(name = "score")
    private int score;

    @ManyToOne
    @JoinColumn(name = "lesson_id", referencedColumnName = "id", nullable = false)
    private Lesson lesson;

    @ManyToOne
    @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false)
    private Student student;

}
