package ru.education.courses.service.http.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Data
public class HttpResponseJson {

    @JsonIgnore
    private boolean isSuccess = false;
    @JsonIgnore
    private final String defaultMessage = "На сервере произошла ошибка. Пожалуйста, повторите позднее или обратитесь в обратную связь.";
    @JsonIgnore
    private final String defaultSuccessMessage = "Успешно";

    private String message = defaultMessage;
    private Map<String,Object> data;
    private ArrayList<HttpResponseError> errors;

    public HttpResponseJson() {
        data = new HashMap<>();
        resetErrors();
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "{\"message\":\"Bad HttpResponseJson object\",\"data\":[],\"errors\":[]}";
        }

    }

    public void resetMessageToDefault() {
        message = defaultMessage;
    }

    public void resetErrors() {
        errors = new ArrayList<>();
        errors.add(getDefaultError());
    }

    public void removeAllErrors() {
        errors = new ArrayList<>();
    }

    public void setSingleError(HttpResponseError httpResponseError) {
        removeAllErrors();
        errors.add(httpResponseError);
        isSuccess = false;
        message = httpResponseError.getMessage();
    }

    public void addError(HttpResponseError httpResponseError) {
        errors.add(httpResponseError);
    }

    private HttpResponseError getDefaultError() {
        return new HttpResponseError(defaultMessage);
    }

    public void clearData() {
        data = new HashMap<>();
    }

    public void setSuccess() {
        isSuccess = true;
        removeAllErrors();
        message = defaultSuccessMessage;
    }

    public void setSuccess(String message) {
        setSuccess();
        this.message = message;
    }

    public void setFailed() {
        isSuccess = false;
        resetErrors();
        resetMessageToDefault();
        clearData();
    }
    public void setFailed(HttpResponseError httpResponseError) {
        setSingleError(httpResponseError);
        clearData();
    }
    public void setFailed(String message) {
        HttpResponseError httpResponseError = new HttpResponseError(message);
        setSingleError(httpResponseError);
        clearData();
    }
    public void setFailed(String message, String element) {
        HttpResponseError httpResponseError = new HttpResponseError(message, element);
        setSingleError(httpResponseError);
        clearData();
    }

    public boolean isSuccess() {
        return isSuccess;
    }

}
