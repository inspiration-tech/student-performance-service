package ru.education.courses.service.http.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class HttpResponseError {

    private String message = "На сервере произошла ошибка. Пожалуйста, повторите позднее или обратитесь в обратную связь.";
    private String element;
    private Map<String,Object> details;

    public HttpResponseError() {
        details = new HashMap<>();
    }

    public HttpResponseError(String message) {
        this.message = message;
        this.details = new HashMap<>();
    }

    public HttpResponseError(String message, String element) {
        this.message = message;
        this.element = element;
        this.details = new HashMap<>();
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "{\"message\":\"Bad HttpResponseJson object\",\"data\":[],\"errors\":[]}";
        }

    }

}
