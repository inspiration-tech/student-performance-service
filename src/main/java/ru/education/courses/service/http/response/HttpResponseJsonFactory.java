package ru.education.courses.service.http.response;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;

@Service
public class HttpResponseJsonFactory {

    public HttpResponseJson createSuccessDefault() {
        HttpResponseJson responseJson = new HttpResponseJson();
        responseJson.setSuccess();

        return responseJson;
    }

    public HttpResponseJson createSuccess(String message) {
        HttpResponseJson responseJson = new HttpResponseJson();
        responseJson.setSuccess(message);

        return responseJson;
    }

    public HttpResponseJson createSuccess(String message, Map<String, Object> data) {
        HttpResponseJson responseJson = new HttpResponseJson();
        responseJson.setSuccess(message);
        responseJson.setData(data);

        return responseJson;
    }

    public HttpResponseJson createSuccess(Map<String,Object> data) {
        HttpResponseJson responseJson = new HttpResponseJson();
        responseJson.setSuccess();
        responseJson.setData(data);

        return responseJson;
    }

    public HttpResponseJson createFailedDefault() {
        return new HttpResponseJson();
    }

    public HttpResponseJson createFailed(HttpResponseError httpResponseError) {
        HttpResponseJson responseJson = new HttpResponseJson();
        responseJson.setSingleError(httpResponseError);

        return responseJson;
    }

    public HttpResponseJson createFailed(String message) {
        HttpResponseJson responseJson = new HttpResponseJson();
        HttpResponseError httpResponseError = new HttpResponseError(message);
        responseJson.setSingleError(httpResponseError);

        return responseJson;
    }

    public HttpResponseJson createFailed(String message, String element) {
        HttpResponseJson responseJson = new HttpResponseJson();
        HttpResponseError httpResponseError = new HttpResponseError(message, element);
        responseJson.setSingleError(httpResponseError);

        return responseJson;
    }

    public HttpResponseJson createFailed(String message, ArrayList<HttpResponseError> errors) {
        HttpResponseJson responseJson = new HttpResponseJson();
        responseJson.setMessage(message);
        responseJson.setErrors(errors);

        return responseJson;
    }

}
