package ru.education.courses.service.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;

@Service
public class CronService {

    private final DataSource dataSource;

    @Autowired
    public CronService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Пересчёт рейтинга студента раз в указанную единицу времени с ожиданием окончания предыдущего пересчёта (см. значение в application.properties)
     */
    @Scheduled(fixedDelayString = "${application.settings.rating-update-frequency}")
    private void updateRatings() {
        try {
            ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(false, false, "UTF-8", new ClassPathResource("sql-queries/update-ratings.sql"));
            resourceDatabasePopulator.execute(dataSource);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
