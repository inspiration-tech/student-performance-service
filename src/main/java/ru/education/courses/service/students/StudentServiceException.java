package ru.education.courses.service.students;

public class StudentServiceException extends Exception {

    public StudentServiceException(String message) {
        super(message);
    }

    public StudentServiceException(String message, Exception exception) {
        super(message, exception);
    }

}
