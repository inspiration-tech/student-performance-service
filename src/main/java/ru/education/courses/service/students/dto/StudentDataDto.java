package ru.education.courses.service.students.dto;

import lombok.Getter;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import ru.education.courses.model.Student;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class StudentDataDto {

    public StudentDataDto() {}

    public StudentDataDto(Student student) {
        ModelMapper mapper = new ModelMapper();
        mapper.map(student, this);
    }

    private Long id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private int groupNumber;

    private List<CourseDataDto> courses = new ArrayList<>();

}
