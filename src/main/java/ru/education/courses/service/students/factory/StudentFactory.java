package ru.education.courses.service.students.factory;

import org.springframework.stereotype.Component;
import ru.education.courses.model.Student;

@Component
public class StudentFactory {

    public Student createStudent(String firstName, String lastName, String patronymic, int groupNumber) {
        Student student = new Student();
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setPatronymic(patronymic);
        student.setGroupNumber(groupNumber);
        student.setActive(true);

        return student;
    }

}
