package ru.education.courses.service.students.factory;

import org.springframework.stereotype.Component;
import ru.education.courses.model.Course;
import ru.education.courses.model.Lesson;

import java.util.Date;

@Component
public class LessonFactory {

    final private int defaultMaximumScore = 5;

    public Lesson createLesson(Long courseId, String name, Date scheduledDate) {
        return this.createLesson(courseId, name, scheduledDate, defaultMaximumScore);
    }

    public Lesson createLesson(Long courseId, String name, Date scheduledDate, int maximumScore) {
        Course course = new Course(courseId);
        Lesson lesson = new Lesson();
        lesson.setCourse(course);
        lesson.setName(name);
        lesson.setScheduledDate(scheduledDate);
        lesson.setMaximumScore(maximumScore);

        return lesson;
    }

}
