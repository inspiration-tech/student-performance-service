package ru.education.courses.service.students.factory;

import org.springframework.stereotype.Component;
import ru.education.courses.model.Course;

import java.util.Date;

@Component
public class CourseFactory {

    public Course createCourse(String name, Date startDate, Date endDate) {
        Course course = new Course();
        course.setName(name);
        course.setStartDate(startDate);
        course.setEndDate(endDate);
        course.setActive(true);

        return course;
    }

}
