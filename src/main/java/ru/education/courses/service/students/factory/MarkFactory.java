package ru.education.courses.service.students.factory;

import org.springframework.stereotype.Component;
import ru.education.courses.model.Lesson;
import ru.education.courses.model.Mark;
import ru.education.courses.model.Student;

@Component
public class MarkFactory {

    public Mark createMark(Long studentId, Long lessonId, int score) {
        Student student = new Student(studentId);
        Lesson lesson = new Lesson(lessonId);
        Mark mark = new Mark();
        mark.setLesson(lesson);
        mark.setStudent(student);
        mark.setScore(score);

        return mark;
    }

}
