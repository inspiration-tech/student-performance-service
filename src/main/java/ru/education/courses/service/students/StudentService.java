package ru.education.courses.service.students;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.education.courses.model.Course;
import ru.education.courses.model.Lesson;
import ru.education.courses.model.Mark;
import ru.education.courses.model.Student;
import ru.education.courses.repository.CourseRepository;
import ru.education.courses.repository.LessonRepository;
import ru.education.courses.repository.MarkRepository;
import ru.education.courses.repository.StudentRepository;
import ru.education.courses.service.students.dto.CourseDataDto;
import ru.education.courses.service.students.dto.StudentDataDto;
import ru.education.courses.service.students.factory.CourseFactory;
import ru.education.courses.service.students.factory.LessonFactory;
import ru.education.courses.service.students.factory.MarkFactory;
import ru.education.courses.service.students.factory.StudentFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class StudentService {

    final private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private StudentRepository studentRepository;
    private CourseRepository courseRepository;
    private LessonRepository lessonRepository;
    private MarkRepository markRepository;
    private StudentFactory studentFactory;
    private LessonFactory lessonFactory;
    private MarkFactory markFactory;
    private CourseFactory courseFactory;

    @Autowired
    public StudentService(
            StudentRepository studentRepository,
            CourseRepository courseRepository,
            LessonRepository lessonRepository,
            MarkRepository markRepository,
            StudentFactory studentFactory,
            LessonFactory lessonFactory,
            MarkFactory markFactory,
            CourseFactory courseFactory
    ) {
        this.studentRepository = studentRepository;
        this.courseRepository = courseRepository;
        this.lessonRepository = lessonRepository;
        this.markRepository = markRepository;
        this.studentFactory = studentFactory;
        this.lessonFactory = lessonFactory;
        this.markFactory = markFactory;
        this.courseFactory = courseFactory;
    }

    /**
     * Получение полных данных по студенту
     */
    public Map<String, Object> getStudentData(Map<String, Object> studentData) throws StudentServiceException {
        StudentDataDto studentDataDto;
        Object studentId = studentData.get("id");

        if (studentId != null) {
            studentDataDto = studentRepository.getStudentDataById(Long.valueOf(studentId.toString()));
        } else {
            String firstName = studentData.get("firstName").toString();
            String lastName = studentData.get("lastName").toString();
            String patronymic = studentData.get("patronymic").toString();

            studentDataDto = studentRepository.getStudentDataByFullName(firstName, lastName, patronymic);
        }

        if (studentDataDto == null) {
            throw new StudentServiceException("Пользователь не найден по указанным данным");
        }

        List<CourseDataDto> courseData = courseRepository.findDTOs(studentDataDto.getId());
        studentDataDto.setCourses(courseData);

        ObjectMapper mapper = new ObjectMapper();
        return mapper.convertValue(studentDataDto, new TypeReference<>(){});
    }

    /**
     * Добавление/обновление данных студента
     */
    public void updateStudent(Map<String, Object> studentData) {
        String firstName = studentData.get("firstName").toString();
        String lastName = studentData.get("lastName").toString();
        String patronymic = studentData.get("patronymic").toString();
        int groupNumber = Integer.parseInt(studentData.get("groupNumber").toString());

        Student student = studentRepository.findByFullName(firstName, lastName, patronymic);

        if (student == null) {
            student = studentFactory.createStudent(firstName, lastName, patronymic, groupNumber);
        } else {
            student.setGroupNumber(groupNumber);
        }

        studentRepository.save(student);
    }

    /**
     * Добавление/обновление данных курса
     */
    public void updateCourse(Map<String, Object> courseData) throws StudentServiceException {
        String courseName = courseData.get("courseName").toString();
        Date startDate;
        Date endDate;

        try {
            startDate = dateFormat.parse(courseData.get("startDate").toString());
            endDate = dateFormat.parse(courseData.get("endDate").toString());
        } catch (ParseException e) {
            throw new StudentServiceException("Некорректный формат даты", e);
        }

        Course course = courseRepository.findByName(courseName);

        if (course == null) {
            course = courseFactory.createCourse(courseName, startDate, endDate);
        } else {
            course.setStartDate(startDate);
            course.setEndDate(endDate);
        }

        courseRepository.save(course);
    }

    /**
     * Добавление/обновление данных занятия
     */
    public void updateLesson(Map<String, Object> lessonData) throws StudentServiceException {
        Long courseId = Long.valueOf(lessonData.get("courseId").toString());
        String lessonName = lessonData.get("lessonName").toString();
        int maximumScore = Integer.parseInt(lessonData.get("maximumScore").toString());
        Date scheduledDate;

        try {
            scheduledDate = dateFormat.parse(lessonData.get("scheduledDate").toString());
        } catch (ParseException e) {
            throw new StudentServiceException("Некорректный формат даты", e);
        }

        Lesson lesson = lessonRepository.findByNameAndCourseId(lessonName, courseId);

        if (lesson == null) {
            lesson = lessonFactory.createLesson(courseId, lessonName, scheduledDate, maximumScore);
        } else {
            lesson.setScheduledDate(scheduledDate);
            lesson.setMaximumScore(maximumScore);
        }

        lessonRepository.save(lesson);
    }

    /**
     * Добавление/обновление оценки
     */
    public void setMark(Map<String, Object> markData) {
        Long studentId = Long.valueOf(markData.get("studentId").toString());
        Long lessonId = Long.valueOf(markData.get("lessonId").toString());
        int score = Integer.parseInt(markData.get("score").toString());

        Mark mark = markRepository.findByStudentIdAndLessonId(studentId, lessonId);

        if (mark == null) {
            mark = markFactory.createMark(studentId, lessonId, score);
        } else {
            mark.setScore(score);
        }

        markRepository.save(mark);
    }

    /**
     * Зачисление студента на какой-л. курс
     */
    public void enrollStudentForCourse(Map<String, Object> enrollmentData) throws StudentServiceException {
        try {
            Long studentId = Long.valueOf(enrollmentData.get("studentId").toString());
            Long courseId = Long.valueOf(enrollmentData.get("courseId").toString());

            enrollStudentForCourse(studentId, courseId);
        } catch (NullPointerException e) {
            String firstName = enrollmentData.get("firstName").toString();
            String lastName = enrollmentData.get("lastName").toString();
            String patronymic = enrollmentData.get("patronymic").toString();
            String courseName = enrollmentData.get("courseName").toString();

            enrollStudentForCourse(firstName, lastName, patronymic, courseName);
        }
    }

    private void enrollStudentForCourse(String firstName, String lastName, String patronymic, String courseName) throws StudentServiceException {
        Student student = studentRepository.findByFullName(firstName, lastName, patronymic);

        if (student == null) {
            throw new StudentServiceException("Студент с указанным именем не найден");
        }

        Course course = courseRepository.findByName(courseName);

        if (course == null) {
            throw new StudentServiceException("Курс с указанным именем не найден");
        }

        enrollStudentForCourse(student, course);
    }

    private void enrollStudentForCourse(Long studentId, Long courseId) throws StudentServiceException {
        Student student = studentRepository.findById(studentId).stream().findFirst()
                .orElseThrow(
                        ()-> new StudentServiceException("Студент с указанным id не найден")
                );


        Course course = courseRepository.findById(courseId).stream().findFirst()
                .orElseThrow(
                        ()-> new StudentServiceException("Курс с указанным id не найден")
                );

        enrollStudentForCourse(student, course);
    }

    private void enrollStudentForCourse(Student student, Course course) {
        student.getCourses().add(course);

        studentRepository.save(student);
    }

}
