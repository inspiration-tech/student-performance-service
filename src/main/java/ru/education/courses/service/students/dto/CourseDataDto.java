package ru.education.courses.service.students.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CourseDataDto {

    private BigInteger id;
    private String courseName;
    private String startDate;
    private String endDate;
    private Integer score;
    private Boolean isPassed;
    private Float percentage;
    private String ratingCreatedAt;

}
