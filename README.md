# СЕРВИС ОТСЛЕЖИВАНИЯ УСПЕВАЕМОСТИ СТУДЕНТОВ

### Документация Swagger по методам API:
http://localhost:8080/swagger-ui/index.html

### Файл для выполнения http-запросов через IntelliJ: 
[http-requests/student-data.http](http-requests/student-data.http)

### Файл стартовых миграций Liquibase:

[migrations/db.changelog-master.xml](src/main/resources/migrations/db.changelog-master.xml)

### SQL-файл, выполняемый кроном для обновления рейтинга:

[sql-queries/update-ratings.sql](src/main/resources/sql-queries/update-ratings.sql)

### Настройка частоты обновления рейтинга студентов:

[application.properties](src/main/resources/application.properties)

_application.settings.rating-update-frequency_
